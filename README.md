5x12 split in lego
==================

  ![M60 split 5x12](pics/5x12s/m60_split.jpg)

  ![M60 split 5x12 leds](pics/5x12s/m60s-leds.jpg)

an ortholinear keyboard set in lego 5x12 split with optional encoders and leds

this is part of a bigger family of ortholinear keyboards in lego see for reference
https://alin.elena.space/blog/keeblego/

current status and more

https://gitlab.com/m-lego/m65

kicad symbols/footprints are in the m65 repo above



status:  tested all ok

* [x] gerbers designed
* [x] firmware
* [x] breadboard tested
* [x] gerbers printed
* [x] board tested

Features:

* 5x12 split
* 1 encoder each side
* led strip
* 5 pins
* stm32f401 or 411 from we act https://github.com/WeActTC/MiniSTM32F4x1
* firmware qmk


the pcb
-------

printed one

  ![M60 split 5x12 left/right pcb](pics/5x12s/m60_split_pcb.png)

left side pcb

  ![M60 split 5x12 left pcb](pics/5x12s/m60-left.png)

right side pcb

  ![M60 split 5x12 left right](pics/5x12s/m60-right.png)

parts
----

* 2xSTM32F401 we act pins
* 60 signal diodes 1N4148 , do-35
* 2 encoders
* 3x4.7kΩ on left hand side R1 and R2 and on right hand side R1
* 2x100kΩ on left hand side R5 and on right hand side R4
* 4x220Ω - these are for leds so you may have to compute the R to match your colours and desired brightness. on left side that are
  R3 and R4 on right side R2 and R3
* 4 leds
* 2 40 pin DIL/DIP sockets whatever you prefer
* 2 JST XH 2.54 3P angle connectors, optionally another too for led strip
* cable to work with the sockets, direct not crossed wires
* led strip 3pins
* 5 pin MX switches 60
* lego 16x16 plates for bottom, and bricks as you please


repo
----

gerbers and kicad files in here  https://gitlab.com/m-lego/m60_split/

firmware
--------

* stm32f401

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m60_split/rev1:default
   make mlego/m60_split/rev1:default:flash
```

* stm32f411

```bash
   git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
   make mlego/m60_split/rev2:default
   make mlego/m60_split/rev2:default:flash
```

other builds
------------

these are Seth's builds (u/modularsynth123456/)

![M60 split 5x12 by Seth](pics/5x12s/m60s_4.jpg)

![M60 split 5x12 by Seth](pics/5x12s/m60s_3.jpg)

![M60 split 5x12 by Seth](pics/5x12s/m60s_1.jpg)

![M60 split 5x12 by Seth](pics/5x12s/m60s_2.jpg)
